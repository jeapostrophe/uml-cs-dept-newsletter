\documentclass{article}

% Variables
\newcommand{\CSIssue}{Summer 2016}
% END Variables

\usepackage[letterpaper, margin=0.75in, bmargin=1cm]{geometry}

\usepackage[latin1]{inputenc}
\usepackage[latin]{babel}
\usepackage{lmodern}
\usepackage{ulem}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{color}
\usepackage[table]{xcolor}

\usepackage{parskip}
\setlength{\parskip}{\medskipamount}
\makeatletter\newcommand{\@minipagerestore}{\setlength{\parskip}{\medskipamount}}

\usepackage{fancyhdr}
\usepackage{multirow}

\usepackage{tabularx}
\newcolumntype{Y}{>{\centering\arraybackslash}X}

\usepackage{rotating}
\usepackage{soul}
\usepackage[overload]{textcase}
\usepackage{dashrule}

\usepackage{hyperref}

\definecolor{cs-blue}{RGB}{2,12,128}
\definecolor{cs-green}{RGB}{205,253,52}

\pagestyle{fancy}
\fancyhf{}
\lhead{\bfseries Page \thepage}
\chead{\bfseries COMPUTER SCIENCE DEPARTMENT NEWSLETTER}
\rhead{\bfseries \MakeTextUppercase{\CSIssue}}
\renewcommand{\headrulewidth}{2pt}% 2pt header rule
\renewcommand{\headrule}{\vspace{-2pt}\hbox to\headwidth{%
    \color{cs-blue}\leaders\hrule height \headrulewidth\hfill}}
\fancyfoot{}

\begin{document}

% First page
\thispagestyle{empty}

% Inside this issue:

\begin{center}
  \def\arraystretch{1.5}
  \begin{tabularx}{\textwidth}{cYYY}
    \multirow{4}{*}{\includegraphics[height=70pt]{img/logo}} & \multicolumn{2}{l}{\cellcolor{cs-blue} \color{white}
      \Large \textbf{Inside this issue:}}
    & {\cellcolor{cs-green} \Large \textbf{\CSIssue}} \\
    ~ & \textbf{Faculty Highlights} & \textbf{Retirements} & \textbf{Publications} \\
    ~ & \textbf{Lab News} & \textbf{Graduations} & \textbf{Alumni News} \\
    ~ & \textbf{Awards} & \textbf{Program Updates} & \textbf{}
  \end{tabularx}
\end{center}

{\color{cs-blue}\hrule\hfill}

% Side bar thing

\begin{tabular}{l!{\color{cs-blue}\vrule width 8pt}r}
  \begin{minipage}[t][0.86\textheight][b]{0.10\textwidth}
    \bfseries\sffamily
    \begin{sideways}\parbox{0.80\textheight}{
        {\large \so{UNIVERSITY OF MASSACHUSETTS LOWELL}}

      ~\\
      
      {\huge Computer Science Department
        Newsletter - \CSIssue}}
    \end{sideways}
\end{minipage} &

% Chair's Message

\begin{minipage}[t][0.80\textheight][t]{0.85\textwidth}

\noindent {\color{cs-blue}{\fontsize{1cm}{3cm}\selectfont\sffamily{\bfseries
    Chair's Message}}\hfill{\fontsize{0.5cm}{1cm}\selectfont ``It's not your grandpa's place''}}

\begin{multicols}{2}

\small

\noindent Dear CS alumni, friends, and colleagues:

\setlength\intextsep{0em}
\begin{wrapfigure}{r}{0.15\textwidth}
  \begin{center}
    \includegraphics[width=0.15\textwidth]{img/haim}
  \end{center}
\end{wrapfigure}

I just ran into an old student, an alumnus, in our local town
library. We live in the same town. We run into each other once in a
while. The last time I had seen him was in 2008, when his daughter and
my son graduated from the town high school.

After we exchanged the usual pleasantaries we ended up talking about
our children. He told me his youngest daughter is interested in
Computer Science, as are his other children, if my memory doesn't fail
me. I asked if he was aware of the major transformation that has taken
place on campus since his graduation. He was not. I asked if he'd be
interested in a visit, a walk around. He said he was. I gave him my
card and invited him to contact me, to come over. I promised I'd show
him around.

Imagine the surprise that's waiting for him. So many more students on
campus (about 17,000, compared to 10,000, maybe 12,000 back then). And
they, on the average, are coming with higher SAT scores, higher high
school grades, and just about any other indicator. Ten new
buildings. And the campus- when I first arrived in Lowell in 1989 the
one thing that bothered me more than anything else was the lack of a
real campus life. At my previous place, the University of
Pennsylvania, I was used to getting out on a sunny afternoon to the
college green and running into friends, colleagues, and other. There
was nothing like that at Lowell in 1989, or 1990, or 2006, for that
matter. But things are different today. I joke that we are on a
transition from ``The University of Massachusetts [at] Lowell'' to
``The City of Lowell at the University of Massachusetts.'' But what it
means for me is that, any day, when my brain is ``pickled,'' I can walk
right out of my office and I am guaranteed to run into students,
colleagues, or friends. (And if I wear one of my bow ties, I know someone
-- usually a student -- will stop her conversation with a bunch of
friends to scream ``great bow tie'' and then continue whatever she was
in the middle of before that.)

Our Computer Science Department is enjoying every little bit of
that boom. No, we do not have ten new buildings. Not even one. (Maybe
we should. But that's another discussion, for another day.) But our
enrollments are ``rolling''; our students are coming in droves, and with
much better credentials than in previous years. (Fall 2016 Freshman
class is expected to be 280 people strong. That's almost as many
undergraduates as we had in all fours years back in 1989 when I
arrived, and for quite a number of years after!)

I am not surprised. On the one hand, there is very little that one can
do today without computers, without computer science. Surely, there is
no science without us. No finance---try to turn off the computers at
your bank. Young people (or is it their parents?) are paying
attention, recognizing that a career in Computer Science is one of the
few guaranteed careers in the foreseeable future. And word has gotten
around that ours is a very strong program, that our graduates get
snapped up for the ever-growing supply of jobs---very well-paying
jobs. Many recruiters have told me that we are the only place they
come back to recruit (quoting some big names---which I won't repeat
here----that they are NOT going to go back to recruit from). Combine
this with our strong affordability and you've got a winning formula.

Our faculty members have substantially increased research funding over
the last few years to around \$4 million per year. Research funding
offers more opportunities for students to be involved in
state-of-the-art research, and to be funded while they are here.

We are very excited about the future. We are looking forward to
welcoming new faculty members. These ``young bloods'' bring excitement
and energy. Not that I would write off the ``old timers'' (I am one of
them!) but the young additions always energize us onward and upward.

Imagine how all of these great things are impacting our campus, our
Department. Not that ``your grandpa's place'' here was that bad, it was
great even then, but it is much greater today---and it's going to be
even greater tomorrow.

Or, better, instead of imagining, come see it with your own eyes. I'd
like to extend to each and every one of you the same invitation I
extended to my former student. Contact me, come over to visit. I'll be
happy to show you around. I promise you will not be
disappointed.

I look forward to hearing from you.

All the best for a great summer, and beyond.

~ 

\noindent Haim Levkowitz, Ph.D. \\
Chair and Associate Professor \\
\texttt{haim@cs.uml.edu} \\
978-934-3654

\end{multicols}

\end{minipage}

\end{tabular}

\newpage

% Commands for non-First Page

\newcommand{\CSArticleXHead}[2]{\noindent {\color{cs-blue}\fontsize{#2}{1cm}\selectfont\hfill \sffamily \bfseries #1 \hfill}}
\newcommand{\CSArticleHead}[1]{\CSArticleXHead{#1}{0.75cm}}
\newcommand{\CSArticleSmallHead}[1]{\CSArticleXHead{#1}{0.5cm}}
\newcommand{\CSArticleByline}[1]{\CSArticleXHead{#1}{0.25cm}}

\newcommand{\CSHRule}{{\color{cs-blue}\hdashrule[0.5ex]{\columnwidth}{2pt}{3mm 3pt 1mm 2pt}}}

% Regular Content

\CSArticleHead{Faculty Highlights}

\begin{multicols}{2}

  \textbf{Yu Cao} (Co-PI), ECE Prof. Vinod Vokkarane (PI), and
  Prof. Yan Luo (Co-PI) have been awarded a \$1M NSF grant to support
  big data networking and analysts for digital health research:
  ``Network Cyberinfrastructure (CI) for Biomedical Informatics
  Innovation.''

  \CSHRule

  \textbf{Yu Cao}, \textbf{Xinwen Fu} (Co-PI), and ECE Prof. Yan Luo
  (PI) have been awarded a \$499K NSF grant, ``Secure Data
  Architecture: STREAMS: Secure Transport and REsearch Architecture
  for Monitoring Stroke Recovery.'' The objective is to develop secure
  and reliable networking and big data analytics for post-stroke
  patient monitoring.

  \CSHRule

  \textbf{Yu Cao} (PI), \textbf{Benyuan Liu} (PI), and Health Science
  Prof. Maria Brunette (PI) have been awarded a \$1.3M NSF/NIH Smart
  and Connected Health (SCH) grant, ``A Sociotechnical Systems
  approach for Improving Tuberculosis Diagnostics Using Mobile Health
  Technologies.'' The goal of this is to develop new deep learning and
  mobile health techniques for improving TB diagnosis.

  \CSHRule

  \textbf{Yu Cao} (PI), together researchers from UMass Lowell
  (\textbf{Benyuan Liu} and Yan Luo), UMass Medical School, and UMass
  Boston, has been awarded \$125,000 by UMass President Office's
  Science and Technology Initiatives Fund (S\&T) to develop and
  establish a center for digital health (CDH). The main objective of
  CDH is to undertake research, evaluate and validate tools, promote
  partnerships with industry, generate technology transfer and promote
  education for the digital health workforce

  \CSHRule

  \textbf{Guanling Chen} gave a tutorial ``Hacking Personal Health
  Behaviors'' at the IEEE on Ubiquitous Intelligence and Computing
  (UIC), Beijing, China, August 2015.

  \CSHRule

  \textbf{Guanling Chen} (Co-PI) received a seed grant from the UMass
  College of Health Science for \$8,000 with Yuan Zhang (PI):
  ``Using Smartwatch to Investigate Health Behaviors of Nightshift
  Nurses.''

  \CSHRule

  \textbf{Tingjian Ge} was invited to give a Distinguished Young
  Lecturer (DYL) talk at Web-Age Information Management (WAIM
  2016). The purposes of the DYL series are: (1) to promote and
  involve active young researchers who have made significant
  progress in establishing themselves with highly visible and
  influential achievements in the web data management community, by
  sharing their experiences and lessons; (2) to set a model example of
  making good technical presentations for student audiences; (3) to
  provide a venue for young researchers to interact and mingle with
  students, thereby attracting good students for possible,
  subsequent collaborative research.

  \CSHRule

  \textbf{Sirong Lin} can't believe that she finished her first year
  of teaching at UML, and her first year of teaching ever! She loves
  the fact that she taught more than \textit{three hundred}
  undergraduate students. She loved talking to and helping all of
  those students. She looks forward to a wonderful second year.

  \CSHRule

  \textbf{Benyuan Liu} will serve as co-chair of \textsc{ieee}
  International Performance Computing and Communications Conference
  2016.

  \CSHRule

  In July 2016, \textbf{Fred Martin} was elected to serve as chair of the board
  of the Computer Science Teachers Association (CSTA), for a two-year
  term commencing in 2017. A subsidiary of the ACM, CSTA
  (\url{csteachers.org}) is membership organization serving over 23,000 K-12
  computer science teachers worldwide.

  \CSHRule

  \textbf{Jay McCarthy} had a fun first-year at UML, returning to
  Lowell after almost ten years. During the year, he had the
  opportunity to teach alongside and learn from Professor Moloney in
  the systems sequence, enjoy participating in faculty hiring, get
  hooked on the Soylent meal-replacement drink, give a TEDx talk, and
  try out local lunch spots with his kids on their half-days.

  \CSHRule

  \textbf{Jay McCarthy} received a grant from the National Science
  Foundation for \$399,994: ``Automated Protocol Design and
  Refinement.''

  \CSHRule

  \textbf{Anna Rumshisky} received a grant from Philips Healthcare for
  \$167,896: ``NLP-driven Predictive Models for Patient Deterioration
  and Therapy Response.''

  \CSHRule

  \textbf{Anna Rumshisky} received a grant from the National
  Institutes of Health for \$367,638: ``Natural language processing
  for characterizing psychopathology.''

  \CSHRule

  \textbf{Anna Rumshisky} received a grant from the Army Research
  Office Core Program for \$377,900: ``Detecting civil conflict and
  information biases in polarized environments in social media.''

  \CSHRule

  \textbf{Kate Saenko}'s team, led by her Ph.D. student Vasili
  Ramanishka won a top prize in the ACM and Microsoft Multimedia Grand
  Challenge.

  \CSHRule

  \setlength\intextsep{0em}
  \begin{wrapfigure}{r}{0.15\textwidth}
    \begin{center}
      \includegraphics[width=0.15\textwidth]{img/kate}
    \end{center}
  \end{wrapfigure}
  \textbf{Kate Saenko} led the \textit{You Code Girl} summer camp at the
  Lowell Public Schools in July.

  \vspace{2em}

  \CSHRule

  \textbf{Holly Yanco}, Bryan Buchholz, PeiChun Kao, Yi Ning Wu, and
  \textbf{Haim Levkowitz} received a grant from UMass President's
  Office Science and Technology Fund for \$123,000: ``NE2R2VE
  Center: Designing Better Robot Systems for People.''

  \CSHRule

  \setlength\intextsep{0em}
  \begin{wrapfigure}{r}{0.15\textwidth}
    \begin{center}
      \includegraphics[width=0.15\textwidth]{img/holly}
    \end{center}
  \end{wrapfigure}
  \textbf{Holly Yanco} was named University Professor this year. She
  was on sabbatical during the spring semester, during which she had the opportunity to visit colleagues in New Zealand, South Korea,
  and the Netherlands. She also attended a workshop organized by
  McGill University in Barbados that focused on testing robots on and
  under the water. Within the US, her trips included a training
  session at NASA's Johnson Space Center in Houston, TX and a visit to
  a puppetry studio outside Portland, OR.

  \CSHRule

  \textbf{Holly Yanco} received a grant from the Defense Advanced
  Research Projects Agency (DARPA) for \$1,641,126: ``Test and
  Evaluation Services for the Fast Lightweight Autonomy Program.''

  \CSHRule

  \textbf{Holly Yanco} (Co-PI) and Taskin Padir (PI) of Northeastern
  University received a grant from NASA for \$250,000 (and a Valkyrie
  humanoid robot valued at \$2,000,000): ``Accessible Testing
  on Humanoid-Robot-R5 and Evaluation of NASA Administered (ATHENA)
  Space Robotics Challenge Tasks.''

  \CSHRule

  \textbf{Holly Yanco} (PI) and Aaron Steinfeld (Co-PI) of Carnegie
  Mellon University received a grant from the National Science
  Foundation for \$184,656: ``EAGER: Collaborative Research:
  Exploring Models for Conveying Imminent Robot Failures to Allow for
  Human Intervention.''
  
\end{multicols}

\CSArticleHead{Karen Daniels' Retirement}

\begin{multicols}{2}

  \setlength\intextsep{0.25em}
  \begin{wrapfigure}{r}{0.15\textwidth}
    \begin{center}
      \includegraphics[width=0.15\textwidth]{img/karen}
    \end{center}
  \end{wrapfigure}
  Prof. Karen Daniels recently retired from the Computer Science
  department after almost 15 years of service.  At her retirement
  dinner she expressed her deep gratitude to the administration, her
  colleagues, the staff, and the students for making UMass Lowell such
  a supportive and enriching environment in which to teach and do
  research.  Her affiliation with UMass Lowell actually started back
  when the school was the University of Lowell and she was a Master's
  student doing work in computer graphics under the guidance of
  Prof. Georges Grinstein.  Fond memories of the faculty and students
  brought her back as a professor.  She most often taught the core
  undergraduate and graduate algorithms class as well as undergraduate
  foundations.  Specialty classes included graduate computational
  geometry and geometric modeling.

  Her research often followed a theme of applied algorithms and
  computational geometry, but also included pattern recognition,
  applied topology, bioinformatics, optimization and, more recently,
  visualization.  Some of her research was supported by NSF and DARPA.
  All the research projects involved students of all levels ranging
  from high school to Ph.D.  Some led to exciting trips with students
  to places like Australia and the beautiful city of Banff in Canada.
  Early in her UMass Lowell faculty career she was a co-principal
  investigator on an NSF grant to guide and mentor students with
  financial need through research projects.  This involved 82
  undergraduate and graduate students, each matched with one or more
  of 22 faculty members drawn from the Electrical and Computer
  Engineering, Computer Science, Mathematics, Chemistry, Biology and
  Plastics Engineering departments.  Prof. Daniels personally mentored
  ten students under this program.

  Prof. Daniels continues to be involved with the University and the
  CS department in her retirement by helping to guide Ph.D. students,
  doing joint visualization research, and collaborative research grant
  proposal submission with colleagues in the Electrical and Computer
  Engineering Department.  She plans to become involved in volunteer
  work in her local middle or high schools.  Much of her focus during
  retirement is on family activities with her husband, two daughters,
  three grandchildren and a large extended family spanning many parts
  of the USA and two continents.  To blend technical work with family,
  she plans to finally read each of her daughters' dissertations from
  cover to cover -- but as a mother, not as a professor!

  As Prof. Daniels enters the next phase of her career, many things in
  the Department, College, and the University are also in flux.  We
  have a new Chancellor, new Provost and a new Dean on the way. A new
  Department Chair and new faculty will be welcomed in as others
  leave. Student enrollments are increasing.  In this time of change,
  she is confident that the Department, College, and University will
  continue to be a wonderful place to teach, learn and do research.
  She feels privileged to be part of this community and looks forward
  to what the future will bring.
  
\end{multicols}

\CSArticleHead{Thank You to Students for My Academic Career} \\
\CSArticleSmallHead{Jesse M. Heines, Professor Emeritus}

\begin{multicols}{2}
  
  \setlength\intextsep{0.25em}
  \begin{wrapfigure}{r}{0.15\textwidth}
    \begin{center}
      \includegraphics[width=0.15\textwidth]{img/jesse}
    \end{center}
  \end{wrapfigure}
My 31$\frac{1}{2}$ years as a college professor are of course filled with memories
both fond and, well, not so fond.  Like any job -- or any relationship,
for that matter -- there have been ups and downs.  But as Gladys Knight
and the Pips sing in \textit{You're the Best Think That Ever Happened to Me}:
``Fate's been kind, the downs have been few.  I guess you could say
that I've been lucky.  Well, I guess you could say that it's all
because of you.''

``You,'' for me, is plural: it's the students who have been in my
classes (about 4,000 by my reckoning!), who have visited me for
advising, or who have simply come to my office to talk.  Some of you
may think that I have given you something valuable of my knowledge,
experience, or judgment, but I assure you that you have given me far
more.  Another song, \textit{Getting to Know You} from ``The King and I'' by
Rodgers \& Hammerstein, expresses this perfectly: ``It's a very ancient
saying, but a true and honest thought: That if you become a teacher,
by your pupils you'll be taught.''

I thank you students for enriching my life both personally and
professionally.  There was no World Wide Web back in 1985 when I began
teaching here, nor even in the early 1990s when I began teaching GUI
programming.  Students showed me things, taught me tricks, and pointed
me toward technologies I had to learn to keep teaching relevant GUI
programming techniques right through my last semester, when we tackled
the MEAN stack for the very first time.

I have no great words of wisdom to pass on as a last formal act of
teaching.  Instead, I encourage you to view what I consider to be the
finest Commencement address I have ever heard, delivered right here at
UMass Lowell in 2007.  In that address U.S. Representative John Lewis
of Georgia spoke about building ``the beloved community.''  You can find
a video of this address posted at
\url{https://uml.ensemblevideo.com/playlist/jheines}, where it is entitled
``Commencement 2007 - Lewis.''  The video image is small and grainy, but
the audio is crystal clear.

Rep. Lewis's words may be even more relevant today than they were
then.  Our current climate of political vitriol is the antithesis of
how Lewis defined beloved: ``not hateful, not violent, not uncaring,
[and] not unkind.''  It is also the antithesis of community, defined
as: ``not separated, not polarized, [and] not locked in struggle.''
Most importantly, Lewis offers the only solution: to effect change we
must all take the responsibility to do so ourselves.  We have to ``find
a way to get in the way'' of the people and situations that divide us.

Quoting the Bill Parrish character at the end of the film Meet Joe
Black, I wish you all ``a life as lucky [and fulfilling] as mine'' has
been.  I thank you all sincerely for being significant contributors to
that fulfillment.

\end{multicols}

\CSArticleHead{Selected Student Researcher Graduations}

\begin{multicols}{2}

  \textbf{Alessandro S. Agnello}, Ph.D. Dissertation Title:
  ``Human-Information Interaction through Analysis of Common
  Heterogeneous Data.'' 2011--2016. Advisor: Haim Levkowitz.

  \textbf{Willie Boag}, graduating senior from the Text Machine Lab,
  has received an NSF Graduate Research Fellowship and has been
  admitted to a Ph.D. program at MIT in Electrical Engineering and
  Computer Science. Advisor: Anna Rumshisky.

  \textbf{Md Gayas Chowdhury}, M.S. Thesis: Secure Web System for
  Classifying Cyber Crimes and Investigation, Defense on Apr. 20,
  2016. Advisor: Xinwen Fu.

  \textbf{Xiang Ding}, Ph.D. Dissertation Title: ``Understanding
  Mobile App Addiction and Promoting Physical Activities.''
  2011--2016. Now at Akamai. Advisor: Guanling Chen.

  \textbf{Ke Huang}, Ph.D. Dissertation Title: ``Unconstrained
  Smartphone Sensing and Empirical Study for Sleep Monitoring and
  Self-Management.'' 2011--2015. Now at Microsoft. Advisor: Guanling
  Chen.

  \textbf{Zheng Li}, Ph.D. Graduated in August 2015 and has been
  working as a Member of Technical Staff at Oracle in Nashua, NH on
  DBMS development. Advisor: Tingjian Ge.

  \textbf{Michael Meding}, MS thesis in Spring 2016, ``Web-Based Model
  Slicing for 3D Printers.'' Advisor: Fred Martin.

  \textbf{Josh Smolinski}, Honors BS in CS thesis in Spring 2016,
  ``Guardian Advanced Rider Protection.'' Advisor: Fred Martin.

  \textbf{Baochen Sun}, Ph.D. Dissertation Title: ``Correlation
  Alignment for Domain Adaptation.'' Advisor: Kate Saenko.

  \textbf{Jing Xu}, Ph.D. Dissertation Title: ``Modeling Choice
  Reaction Time and Predicting User Frustration for Mobile App
  Interactions.'' 2011--2016. Now at Wayfairs. Advisor: Guanling Chen.

  \textbf{Xu Ye}, Ph.D. Dissertation Title: ``Automatic Eating
  Detection using Wearable Devices.'' 2011--2016. Now at
  Autodesk. Advisor: Guanling Chen.

\end{multicols}

\newpage

\CSArticleHead{Brianna Gainley}

\begin{multicols}{2}

  \textit{In last year's newsletter, we carried Bri's story about
    ``Fighting Back from Bone Cancer.''}

  \setlength\intextsep{0em}
  \begin{wrapfigure}{r}{0.20\textwidth}
  \begin{center}
    \includegraphics[width=0.20\textwidth]{img/bri-3}
  \end{center}
  \end{wrapfigure}
  Brianna Theresa Gainley of Burlington, at the age of 24, passed away
  on Saturday, April 23, 2016, after a valiant five year battle
  against osteosarcoma. Brianna was born in Winchester and was a
  lifelong resident of Burlington. She was a 2010 graduate of
  Burlington High School, and after high school, Brianna attended the
  University of Massachusetts on a full Commonwealth Scholarship. It
  was in her freshman year of college when Brianna's symptoms first
  appeared. A sharp pain in her right knee was eventually diagnosed as
  osteosarcoma. As the cancer progressed and Brianna was faced with
  many challenges, she leveraged her inner strength to remain positive
  and overcome the obstacles she was confronted with. After Brianna
  had to have her leg amputated early last year, she learned to walk
  again with a prosthetic leg and finished the $\frac{1}{4}$ mile
  ``survivor's lap'' at last summer's Relay for Life at Burlington
  High School.

  Throughout her ordeal, Brianna continued to pursue her college
  education and enjoy life. She had a very creative mind and enjoyed
  painting, sketching, music, reading, arts and crafts, video games,
  anime, computers, and programming. Brianna loved being outdoors, and
  enjoyed bicycling, being on the water, and boating on the Merrimack
  River. She was a member of a robotics team at UMass Lowell that won
  an award at a national NASA robotics competition in Houston. After
  Brianna received her terminal diagnosis several weeks ago, UML held
  a special graduation ceremony where Brianna received her degree and
  was introduced as the first graduate of the Class of 2016.

  Brianna had a special spirit that prevailed during her life. She
  didn't let her illness define her, limit her, or keep her from
  accomplishing her goals. Brianna will be fondly remembered for many
  things, including her strength, perseverance, determination and
  courage. Although her life was much too short, Brianna lived life to
  the fullest during the short time that she had on earth. She made
  many friends, traveled, earned her college degree, found true love,
  and enriched the lives of everyone who knew her.

  Brianna was the loving fianc\'ee of Ryan Hart. She was the cherished
  and much loved daughter of Maureen (Duggan) and Frank Gainley,
  Jr. of Burlington. She was the older and loving sister of Tayla,
  Marlea, and Shaylinn Gainley all of Burlington. She was the
  granddaughter of Judy Gainley of Burlington, the late Frank Gainley,
  and the late Robert and Theresa Duggan. Brianna was the niece of
  Lisa and Tom Dimino of Peabody, Karen Duggan of Athol, Kevin and
  Joan Duggan of MS, and Shawn Duggan of CO. She was also survived by
  many friends, classmates, and colleagues.

  In accordance with Brianna's wishes, funeral services were held in
  private. In lieu of flowers, Brianna wanted to help others who have
  osteosarcoma and requested that donations in her memory be
  considered and made to:
  \url{http://www.curesarcoma.org/in-memory-of-brianna-gainley}

  \textit{For an article about Brianna's graduation ceremony, visit:
    \url{https://www.uml.edu/News/stories/2016/Brianna-Gainley-Graduation.aspx}.}
  
\end{multicols}

\begin{center}
\begin{tabular}{cc}
  \includegraphics[width=0.45\textwidth]{img/bri-2} &
  \includegraphics[width=0.45\textwidth]{img/bri-1}
\end{tabular}
\end{center}

\newpage

\CSArticleHead{Selected Faculty and Student Publications}

\begin{multicols}{2}
  \small
  
  Yang Gao, Ning Zhang, Honghao Wang, Xiang Ding, Xu Ye,
  \textbf{Guanling Chen}, and \textbf{Yu Cao}. \textit{iHearFood:
    Eating Detection Using Commodity Bluetooth Headsets}. IEEE CHASE,
  2016.

  Chang Liu, \textbf{Yu Cao}, and \textbf{Guanling
    Chen}. \textit{DeepFood: Deep Learning-based Food Image
    Recognition for Computer-aided Dietary Assessment}. IEEE ICOST,
  2016.

  Xu Ye, \textbf{Guanling Chen}, and \textbf{Yu
    Cao}. \textit{Assisting Food Journaling with Automatic Eating
    Detection}. ACM CHI, 2016.

  Xu Ye, \textbf{Guanling Chen}, and \textbf{Yu
    Cao}. \textit{Automatic Eating Detection Using Head-Mount and
    Wrist-Worn Accelerometers}. International Conference on
  E-health Networking, Application \& Services (HealthCom), 2015.

  \textit{Design of Reciprocal Recommendation Systems for Online
    Dating} Peng Xia, Shuangfei Zhai, \textbf{Benyuan Liu}, Yizhou
  Sun, \textbf{Cindy Chen} Journal of Social Network Analysis and
  Mining (SNAM), Springer, 2016.

  Chunyao Song, \textbf{Tingjian Ge}, \textbf{Cindy Chen}, \textbf{Jie
    Wang}. \textit{Event Pattern Matching over Graph Streams}. In the
  VLDB Endowment (PVLDB journal), Volume 8, Issue
  4, and International Conference on Very Large Data Bases
  (VLDB 2015), 2015.

  Edwin D. Boudreaux, Andrew Fischer, Brianna Haskins, Zubair Zafar,
  \textbf{Guanling Chen}, and Sneha Shah. \textit{Optimizing Usability
    of Electronic Patient Reported Outcomes Through Iterative Testing
    and Modification}, Journal of Medical Internet Research Human
  Factors (JHF), 2015.

  Haoyi Xiong, Daqing Zhang, \textbf{Guanling Chen}, Leye Wang, and
  Vincent Gauthier. \textit{iCrowd: Near-Optimal Task Allocation for
    Piggyback Crowdsensing}. ACM Transaction of Mobile Computing
  (TMC), 2015.

  Yuan Zhang, Anya Peters, and \textbf{Guanling Chen}. \textit{Sleep
    quality and mental disorders among college nursing students: The
    mediating role of perceived stress and coping styles}. APHA 2016
  Annual Meeting \& Expo, 2016.

  Jing Xu, Xiang Ding, Ke Huang, and \textbf{Guanling
    Chen}. \textit{Unsupervised Detection of Abnormal Moments for
    Usability Testing of Mobile Apps}. ACM CHI, 2016.

  Xiang Ding, Jing Xu, \textbf{Guanling Chen}, and Chenren
  Xu. \textit{Beyond Smartphone Overuse: Identifying Addictive Mobile
    Apps}. ACM CHI, 2016.

  Ke Huang, Xiang Ding, Jing Xu, \textbf{Guanling Chen}, and Wei
  Ding. \textit{Monitoring Sleep and Detecting Irregular Nights
    through Unconstrained Smartphone Sensing}. International
  Conference on Ubiquitous Intelligence and Computing (UIC), 2015.

  Jiaxu Sun, Yongchao Li, Linzhang Wang, Xuandong Li, Xiaoxiao Ma,
  Jing Xu, and \textbf{Guanling Chen}. \textit{Controlling Smart TVs
    using Touch Gestures on Mobile Devices}. International Symposium
  on UbiCom Frontiers - Innovative Research, Systems and Technologies
  (UFirst), 2015.

  Zhen Ling, Junzhou Luo, Qi Chen, Qinggang Yue, Ming Yang, Wei Yu,
  \textbf{Xinwen Fu}. \textit{Secure Fingertip Mouse for Mobile
    Devices}, in IEEE International Conference on Computer
  Communications (INFOCOM), 2016.

  Xian Pan, Zhen Ling, Aniket Pingley, Wei Yu, Nan Zhang, Kui Ren,
  \textbf{Xinwen Fu}. \textit{Password Extraction via Reconstructed
    Wireless Mouse Trajectory}. IEEE Transactions on Dependable
  Security Computing (TDSC), 13(4): 461-473, 2016
  
  Lijian Wan and \textbf{Tingjian Ge}. \textit{Event Regularity and
    Irregularity in a Time Unit}. IEEE International Conference on
  Data Engineering (ICDE 2016), 2016.

  Honggang Zhang, \textbf{Benyuan Liu}, Hengky Susanto, Guoliang Xue,
  Tong Sun. \textit{Incentive Mechanism for Proximity-based Mobile
    Crowd Service Systems}. IEEE Infocom, 2016.

  Steven Coughlin, Herpreet Thind, \textbf{Benyuan Liu}, Nicole
  Champagne, Molly Jacobs, Rachael I Massey. \textit{Smartphone
    Applications for Preventing Cancer through Educational and
    Behavioral Interventions: State of the Art and Remaining
    Challenges}. JMIR mHealth and uHealth, 2016.

  Steven S. Coughlin, Herpreet Thind, \textbf{Benyuan Liu}, Candy
  Wilson.  \textit{Towards Research-Tested Smartphone Applications for
    Preventing Breast Cancer}.  AME mHealth Journal, 2016.
  
  Lijun Ni, Diane Schilder, Mark Sherman, and \textbf{Fred
    Martin}. \textit{Computing with a community focus: outcomes from
    an app inventor summer camp for middle school students}. Journal
  of Computing Sciences in Colleges, 31(6), 2016, p. 82--89.

  Samantha Michalka, James Dalphond, and \textbf{Fred
    Martin}. \textit{Inquiry Learning with Data and Visualization in
    the STEM Classroom}. Society for Information Technology \& Teacher
  Education International Conference, 2016

  \textbf{Fred G. Martin} \textit{Computational Thinking is a
    model-eliciting activity}. CSTA Voice 12(1), March 2016, p. 8.

  Helen H. Hu, Cecily Heiner, and \textbf{Jay
    McCarthy}. \textit{Deploying Exploring Computer Science
    Statewide}. SIGCSE 2016.

  \textbf{Jay McCarthy}, Burke Fetscher, Max New and Robert Bruce
  Findler. \textit{A Coq Library For Internal Verification of
    Running-Times}. FLOPS 2016.

  \textbf{Jay McCarthy}. \textit{Bithoven: G\"odel Encoding of Chamber
    Music and Functional 8-Bit Audio Synthesis}. FARM 2016.

  Willie Boag, Renan Campos, \textbf{Kate Saenko}, \textbf{Anna
    Rumshisky}. \textit{MUTT: Metric Unit TesTing for Language
    Generation Tasks}. ACL 2016.

  Momotaz Begum, Richard Serna, and \textbf{Holly Yanco}. \textit{Are
    Robots Ready to Deliver Autism Interventions? A Comprehensive
    Review}.  International Journal of Social Robotics, 8(2):157-181,
  April 2016.

  \textbf{Holly A. Yanco}, Munjal Desai, Jill L. Drury, and Aaron
  Steinfeld. \textit{Methods for Developing Trust Models for
    Intelligent Systems}. In Robotic Intelligence and Trust in
  Autonomous Systems, edited by Ranjeev Mittu, Don Sofge, Alan Wagner,
  and W.F. Lawless, 2016.

  Daniel J. Brooks, Katherine M. Tsui, and \textbf{Holly
    A. Yanco}. \textit{Methods for Evaluating and Comparing the Use of
    Haptic Feedback in Human-Robot Interaction with Ground-Based
    Mobile Robots}. Journal of Human-Robot Interaction, 4(1): 3-29,
  2015.

  Abraham Shultz, Sangmook Lee, Thomas B. Shea, and \textbf{Holly
    A. Yanco}. \textit{Biological and Simulated Neuronal Networks Show
    Similar Competence on a Visual Tracking Task}. IEEE International
  Conference on Development and Learning and on Epigenetic Robotics
  (IEEE ICDL-EPIROB), 2015.

  \textbf{Holly A. Yanco}, Adam Norton, Willard Ober, David Shane,
  Anna Skinner, and Jack Vice. \textit{Analysis of Human-Robot
    Interaction at the DARPA Robotics Challenge Trials}. Journal of
  Field Robotics, 32(3): 420-444, May 2015.

  Daniel J. Brooks, Eric McCann, Jordan Allspaw, Mikhail Medvedev, and
  \textbf{Holly A. Yanco}. \textit{Sense, Plan, Triple Jump}. IEEE
  International Conference on Technologies for Practical Robot
  Applications (TePRA), 2015.

\end{multicols}

\CSArticleHead{Center and Program News}

\begin{multicols}{2}

\CSArticleSmallHead{Center for Cyber Forensics}
  
The University of Massachusetts Lowell has been designated as a
National Center of Academic Excellence in Cyber Defense Research
(CAE-R).

We are happy to be able to meet the increasing demands of the program
criteria and will serve the nation well in contributing to the
protection of the National Information Infrastructure. The Presidents'
National Strategy to Secure Cyberspace, 14 February 2003 and the
International Strategy for Cyberspace, May 2011, address the
critical shortage of professionals with these skills and highlight
the importance of higher education as a solution to defending
America's cyberspace.

\CSHRule

\CSArticleSmallHead{NERVE Center}

\setlength\intextsep{0em}
\begin{wrapfigure}{l}{0.2\textwidth}
  \begin{center}
    \includegraphics[width=0.2\textwidth]{img/valkyrie}
  \end{center}
\end{wrapfigure}
A collaborative team composed of Northeastern University (Taskin Padir
and Robert Platt) and UMass Lowell (Holly Yanco) receive in May one of
two humanoid robot awards from NASA, an R5 ``Valkyrie'' robot,
building off of the teams' work with humanoid robots at the DARPA
(Defense Advanced Research Projects Agency) Robotics Challenge. ``We
look forward to hosting the 6 foot tall `Valkyrie,' at UMass Lowell's
NERVE Center, as we work with our collaborators at Northeastern,''
says Professor Yanco. The second award will go to MIT. Researchers at
the NERVE Center will be developing a set of tasks and test methods,
in addition to the extreme environments already created, for Valkyrie
to experience in anticipation of the 2017 Space Robotics Challenge
(SRC).

In other NERVE news, we turned the old F-15 hangar at Joint Base Cape
Cod into a giant warehouse to test autonomous UAVs for DARPA's Fast
Lightweight Autonomy program.

Visit \url{nerve.uml.edu} for awesome videos about both of these
items.

\CSHRule

\CSArticleSmallHead{Text Machine Lab}

Our lab will run an international competition on humor detection at
the main venue for shared tasks on semantic evaluation for natural
language processing (International Workshop on Semantic Evaluation,
SemEval-2017). We will run Task 6: \#HashtagWars: Learning a Sense of
Humor

Our lab will also run a Clinical Natural Language Processing workshop
in Osaka, Japan, focusing on automated understanding of clinical text
to improve patient outcomes.

\CSHRule

\CSArticleSmallHead{Undergraduate Program Update} \\
\CSArticleByline{By David Adams, Undergraduate Coordinator}

This year we were very pleased to welcome a new lecturer to our first-
and second-year teaching team: Dr. Sirong Lin.  Dr. Lin received her
graduate degrees from Virginia Tech studying parallel thinking
pedagogy and worked in industry for a few years as a user-interaction
designer and software engineer before turning to academia. She brings
a deep passion for education and enjoys working with the students.
The students in her classes have given her great reviews, and she is
also contributing to the Department as Assistant Undergraduate
Coordinator.

We added the Assistant Undergraduate Coordinator position to help
manage the huge growth in incoming students over the last four years.
That growth increased from 11\% three years ago to a whopping 42\%
last year. We are on track for another 40\% increase this year and
will begin educating the largest incoming class in over twenty years.

We have additional new faculty joining us in the Fall 2016 semester,
including Asst. Prof. Wenjin Zhou, who will be taking over the popular
GUI Programming sequence from retiring Prof. Jesse Heines.  A new
project sequence in Mobile Computing and Security is also being added,
to be taught by Assoc. Prof. Guanling Chen.

Our increased enrollment has also justified the addition of another
lecturer and another tenure-track professor.  We hope to have people
on board to fill these positions to further address our increased
teaching needs.

The Department will undergo ABET Accreditation review with a visit
from the accreditation team in November of this year demonstrating the
commitment of the department to continuous improvement and program
excellence.

\end{multicols}

\newpage

\hfill \begin{minipage}[t][0.10\textheight][t]{0.3\textwidth}Please contact Assoc. Prof. Jay McCarthy at \href{mailto:jmccarth@cs.uml.edu}{jmccarth@cs.uml.edu} with
alumni news for next issue.\end{minipage}

\vspace{-0.15\textheight}
\includegraphics{img/Newsletter-From}

\CSHRule

\vfill

\includegraphics[width=\textwidth]{img/Begging}
\vspace{0cm}

\end{document}
